import requests
import logging

from flask import Flask, request, render_template, redirect, url_for
from flask_sock import Sock
from pymongo import MongoClient
from stockportal_be.app_config.app_config import AppConfig
from stockportal_be.app_constants.app_constants import BAD_REQUEST_ERROR_METHOD_NOT_ALLOWED, INTERNAL_SERVER_ERROR_GENERIC
from stockportal_be.stock_api.alphavantage import AlphaVantageAPI
from stockportal_be.stock_api.background_thread import TASKS_QUEUE, BackgroundThreadFactory

logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)

app = Flask(__name__)
sock = Sock(app)

def start():
    app.run(host='0.0.0.0', threaded=True, port=5001)

def stop():
    resp = requests.get('http://localhost:5001/shutdown')

@app.route("/shutdown", methods=['GET'])
def shutdown():
    shutdown_func = request.environ.get('werkzeug.server.shutdown')
    if shutdown_func is None:
        raise RuntimeError('Not running werkzeug')
    shutdown_func()
    return "Shutting down..."


@app.route("/")
def index():
    return render_template('index.html')

# @sock.route("/price")
# def get_price(ws):
#     while True:
#         symbol = ws.receive()
#         time.sleep(20)
#         info = stock_api_obj.get_price(symbol)
#         # res = str(info)
#         # TASKS_QUEUE.put(symbol)
#         ws.send(info)

@app.route("/register", methods=['POST'])
def register_email():
    try:
        if request.method == 'POST':
            email = request.form["content_email"]
            api_key = request.form["content_api_key"]

            existing_user = mongo_client.db.user.find_one(
                {
                    "email": email
                }
            )

            if existing_user is None:
                mongo_client.db.user.insert_one(
                    {
                        "email": email,
                        "api_key": api_key,
                    }
                )

                return redirect(url_for('login', email=email, is_api_key_changed=False, is_new_user=True))

            else:
                if 'api_key' not in existing_user or existing_user['api_key'] != api_key:
                    mongo_client.db.user.update_one(
                        {
                            '_id': existing_user['_id']
                        },
                        {
                            '$set': {
                                'api_key': api_key
                            }
                        }, 
                        upsert=False
                    )

                    print("UPDATED KEY")

                    return redirect(url_for('login', email=email, is_api_key_changed=True, is_new_user=False))

                else:
                    return redirect(url_for('login', email=email, is_api_key_changed=False, is_new_user=False))

        else:
            return BAD_REQUEST_ERROR_METHOD_NOT_ALLOWED

    except Exception as e:
        return str(e)


@app.route("/login", methods=['POST', 'GET'])
def login():
    try:
        # POST REQUEST

        if request.method == 'POST':
            symbol = request.form["content_symbol"]
            email = request.form["content_email"]
            qty = request.form["content_qty"]

            if qty.isnumeric() is False:
                qty = 0

            print(symbol, email)
            existing_email_symbol = mongo_client.db.stock_subscriptions.find_one(
                {
                    "email": email,
                    "symbol": symbol,
                }
            )

            if existing_email_symbol is None:
                mongo_client.db.stock_subscriptions.insert_one(
                    {
                        "email": email,
                        "symbol": symbol,
                        "qty": qty,
                        "price": 0,
                        "last_fetched": "",
                    }
                )

            return redirect(url_for('login', email=email, is_api_key_changed=False, is_new_user=False))


        # GET REQUEST

        args = request.args
        email = args.get('email')
        is_api_key_changed = args.get('is_api_key_changed')
        is_new_user = args.get('is_new_user')

        account_info = mongo_client.db.user.find_one(
            {
                "email": email
            }
        )

        existing_email_symbols = mongo_client.db.stock_subscriptions.find(
            {
                "email": email,
            }
        )

        # emails = list(emails)

        return render_template('stock_info.html', account_info=account_info, existing_email_symbols=existing_email_symbols, is_api_key_changed=is_api_key_changed, is_new_user=is_new_user)

    except Exception as e:
        return str(e)


# Setup Application Configuration
# config_obj = AppConfig()
# if config_obj.validate() == False:
#     raise Exception("NO API KEY DEFINED")

# Setup Application Stock API Object
# stock_api_obj = AlphaVantageAPI(config_obj)

# Setup Application Database Client
# mongo_client = MongoClient('localhost', 27017, username='username', password='password')
mongo_client = MongoClient('localhost', 27017)

# Setup Background Thread
stock_price_api_thread = BackgroundThreadFactory.create('stock_price_api', mongo_client)
stock_price_api_thread.start()