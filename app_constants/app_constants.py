# Messages from Alpha Vantage
ALPHA_VANTAGE_API_LIMIT_EXCEEDED_VERIFY = 'Thank you for using Alpha Vantage! Our standard API call frequency is 5 calls per minute and 500 calls per day.'


# Error messages Sent to Server
INTERNAL_SERVER_ERROR_GENERIC = "Internal Server Error. Create Dev Ticket"
INTERNAL_SERVER_ERROR_API_LIMIT_EXCEEDED = "Internal Server Error: Too many requests per minute"

BAD_REQUEST_ERROR_METHOD_NOT_ALLOWED = "BAD METHOD: The URL does not allow used HTTP method"