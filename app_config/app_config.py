import os

from dotenv import load_dotenv
from os.path import join, dirname


class AppConfig:

    def __init__(self) -> None:
        dotenv_path = join(os.path.dirname(dirname(__file__)), '.env')
        load_dotenv(dotenv_path)

        self.API_KEY = None
        self.load_config()
        return

    def load_config(self) -> None:
        self.API_KEY = os.getenv("API_KEY")

    def validate(self) -> bool:
        if self.API_KEY is None:
            return False

        return True