from abc import ABC, abstractmethod

class BaseStockAPI(ABC):
    @abstractmethod
    def get_price(self, symbol):
        pass