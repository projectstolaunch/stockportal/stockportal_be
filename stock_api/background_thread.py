import logging
import queue
import threading
import time
from queue import Queue
from abc import abstractmethod, ABC
from typing import Dict
from datetime import datetime
from stockportal_be.app_constants.app_constants import INTERNAL_SERVER_ERROR_API_LIMIT_EXCEEDED

from stockportal_be.stock_api.alphavantage import AlphaVantageAPI

TASKS_QUEUE = Queue()


class BackgroundThread(threading.Thread, ABC):
    def __init__(self):
        super().__init__()
        self._stop_event = threading.Event()

    def stop(self) -> None:
        self._stop_event.set()

    def _stopped(self) -> bool:
        return self._stop_event.is_set()

    @abstractmethod
    def startup(self) -> None:
        """
        Method that is called before the thread starts.
        Initialize all necessary resources here.
        :return: None
        """
        raise NotImplementedError()

    @abstractmethod
    def shutdown(self) -> None:
        """
        Method that is called shortly after stop() method was called.
        Use it to clean up all resources before thread stops.
        :return: None
        """
        raise NotImplementedError()

    @abstractmethod
    def handle(self) -> None:
        """
        Method that should contain business logic of the thread.
        Will be executed in the loop until stop() method is called.
        Must not block for a long time.
        :return: None
        """
        raise NotImplementedError()

    def run(self) -> None:
        """
        This method will be executed in a separate thread
        when start() method is called.
        :return: None
        """
        self.startup()
        while not self._stopped():
            self.handle()
        self.shutdown()


class StockPriceAPIThread(BackgroundThread):
    def __init__(self, mongo_client):
        super().__init__()
        self.mongo_client = mongo_client

    def startup(self) -> None:
        logging.info('StockPriceAPIThread started')

    def shutdown(self) -> None:
        logging.info('StockPriceAPIThread stopped')

    def handle(self) -> None:
        try:
            logging.info("Starting Stock Price API Thread")

            while True:
                logging.info("Fetching all users")

                accounts = self.mongo_client.db.user.find()
                curr_time = datetime.now()

                for account in accounts:
                    print(account)
                    print(account["email"])
                    logging.info("Checking for " + str(account["email"]))

                    existing_email_symbols = self.mongo_client.db.stock_subscriptions.find(
                        {
                            "email": account["email"],
                        }
                    )

                    if existing_email_symbols is None:
                        continue

                    stock_api_obj = AlphaVantageAPI(account["api_key"])

                    for symbol in existing_email_symbols:
                        
                        logging.info("Checking for " + str(account["email"]) + " : " + str(symbol["symbol"]))

                        # Check if price needs to be fetched
                        logging.info("Setting toFetch as False")
                        to_fetch = False

                        last_fetched_time = symbol["last_fetched"]
                        if isinstance(last_fetched_time, str) == True:
                            logging.info("Setting toFetch as True cus of length")
                            to_fetch = True

                        else:
                            diff = last_fetched_time - curr_time
                            diff_minutes = diff.seconds/60
                            
                            if diff_minutes > 15:
                                to_fetch = True
                                logging.info("Setting toFetch as True cus of difference")

                        # Fetch new price and update database
                        if to_fetch == True:
                            logging.info("Fetching price from API")
                            info = stock_api_obj.get_price(symbol["symbol"])

                            if info == INTERNAL_SERVER_ERROR_API_LIMIT_EXCEEDED:
                                logging.info("Server returned limit exceeded")
                                time.sleep(65)
                                logging.info("Wait of 65 seconds over. Calling again")
                                info = stock_api_obj.get_price(symbol["symbol"])

                            try:
                                tmp_info = float(info)
                                logging.info("info can be converted to numeric")
                            except Exception as e:
                                logging.info("info is not numeric")
                                logging.info(info)
                                break
                            
                            logging.info("info is numeric. Updating database")
                            self.mongo_client.db.stock_subscriptions.update_one(
                                {
                                    '_id': symbol['_id']
                                },
                                {
                                    '$set': {
                                        'price': float(info),
                                        'last_fetched': curr_time,
                                    }
                                }, 
                                upsert=False
                            )

                            logging.info("Price was updated")
                
                diff_minutes = 1
                
                while diff_minutes < 2:
                    new_curr_time = datetime.now()
                    diff = new_curr_time - curr_time
                    diff_minutes = diff.seconds/60
                    time.sleep(60)

            # task = TASKS_QUEUE.get(block=False)
            # send_notification(task)
            # logging.info(f'Price for {task} was fetched.')

        except Exception as e:
            logging.error("BACKGROUND WORKER CRASHED")
            logging.error(e)
            raise e


class BackgroundThreadFactory:
    @staticmethod
    def create(thread_type: str, mongo_client) -> BackgroundThread:
        if thread_type == 'stock_price_api':
            return StockPriceAPIThread(mongo_client)

        # if thread_type == 'some_other_type':
        #     return SomeOtherThread()

        raise NotImplementedError('Specified thread type is not implemented.')