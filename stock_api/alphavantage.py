import requests
from stockportal_be.app_constants.app_constants import ALPHA_VANTAGE_API_LIMIT_EXCEEDED_VERIFY, INTERNAL_SERVER_ERROR_API_LIMIT_EXCEEDED, INTERNAL_SERVER_ERROR_GENERIC
from stockportal_be.stock_api.base_stock_api import BaseStockAPI


class AlphaVantageAPI(BaseStockAPI):

    def __init__(self, api_key) -> None:
        super().__init__()

        self.ALPHA_VANTAGE_URL = "https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol="
        self.api_key = api_key

    def get_price(self, symbol) -> str:
        url = self.ALPHA_VANTAGE_URL + symbol + "&apikey=" + self.api_key
        r = requests.get(url)

        data = r.json()
        try:
            price = data["Global Quote"]["05. price"]
            return str(price)

        except Exception as e:
            if ALPHA_VANTAGE_API_LIMIT_EXCEEDED_VERIFY in str(data):
                return INTERNAL_SERVER_ERROR_API_LIMIT_EXCEEDED
            return INTERNAL_SERVER_ERROR_GENERIC